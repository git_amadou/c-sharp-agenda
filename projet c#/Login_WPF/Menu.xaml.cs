﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Vue
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Menu : Window
    {

        public Menu()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)

        {
            ListeContacts contacts = new ListeContacts();
            contacts.Show();
            this.Close();

            //MainWindow ss = new MainWindow();

            //this.Visibility = Visibility.Hidden;

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void Button_Ajouter(object sender, RoutedEventArgs e)
        {
            Ajouter aj = new Ajouter();
            aj.Show();
            //aj.Visibility = System.Windows.Visibility.Visible;

            this.Close();
        }



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();

        }



        private void Button_ContactsActuels(object sender, RoutedEventArgs e)
        {
            ListeContacts lc = new ListeContacts();
            lc.Show();
            this.Close();

        }
    }
}