﻿using System.Collections.Generic;
using Modele;
using System.Windows;
using System.Windows.Controls;

namespace Vue {
    public partial class ListeContacts : Window {
        public ListeContacts() {
            InitializeComponent();
            listContact.ItemsSource = Bll.Chercher.ChargerLesContacts(Vue.SeConnecter.utilisateurEnCours);

        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            Menu ajout = new Menu();
            ajout.Show();
            this.Close();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e) {
            
            Bll.Supprimer.SuprimerContact((Contact)listContact.SelectedItem, SeConnecter.utilisateurEnCours);
            listContact.ItemsSource = Bll.Chercher.ChargerLesContacts(Vue.SeConnecter.utilisateurEnCours);
        }

        private void Button_Click_5(object sender, RoutedEventArgs e) {
            Ajouter aj = new Ajouter();
            Close();
            aj.Show();
        }
    }

}
