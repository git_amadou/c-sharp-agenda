﻿using System.Windows;
using System.Text.RegularExpressions;
using Bll;
using Modele;
namespace Vue
{
    public partial class InscriptionUtilisateur : Window
    {
        // Utilisateur nv;
        public InscriptionUtilisateur()
        {

            InitializeComponent();

        }
        public void Renitialiser()
        {
            textBoxFirstName.Text = "";
            textBoxLastName.Text = "";
            textBoxEmail.Text = "";
            textBoxAddress.Text = "";
            passwordBox1.Password = "";
            passwordBoxConfirm.Password = "";
        }
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            SeConnecter login = new SeConnecter();
            login.Show();
            Close();
        }
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Renitialiser();
        }


        private void button3_Click(object sender, RoutedEventArgs e) {
            MainWindow mn = new MainWindow();
            Close();
            mn.Show();
        }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxEmail.Text.Length == 0)
            {
                erreurrmessage.Text = "veuillez entrer un e-mail .";
                textBoxEmail.Focus();
            }
            else if (!Regex.IsMatch(textBoxEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                erreurrmessage.Text = "veuillez entrer un e-mail valide.";
                textBoxEmail.Select(0, textBoxEmail.Text.Length);
                textBoxEmail.Focus();
            }
            else
            {

                string password = passwordBox1.Password;
                if (passwordBox1.Password.Length == 0)
                {
                    erreurrmessage.Text = "Entere un mot de passe .";
                    passwordBox1.Focus();
                }
                else if (passwordBoxConfirm.Password.Length == 0)
                {
                    erreurrmessage.Text = "Entrer la Confirmation du mot de passe.";
                    passwordBoxConfirm.Focus();
                }
                else if (passwordBox1.Password != passwordBoxConfirm.Password)
                {
                    erreurrmessage.Text = "la confirmation du mot de passe doit etre identique au mdp";
                    passwordBoxConfirm.Focus();
                }
                else
                {
                    erreurrmessage.Text = "";
                    // appele de la methode du BLL
                    Bll.Ajouter.AjouterNouveauUtilisateur(new Utilisateur
                    {
                        Nom = textBoxFirstName.Text,
                        Prenom = textBoxLastName.Text
                    ,
                        Email = textBoxEmail.Text,
                        Password = password,
                        Adresse = textBoxAddress.Text
                    });
                    //

                    succesmessage.Text = "Felicitation! Votre compte a bien ete enregistrer.";
                    Renitialiser();
                }
            }
        }
    }
}