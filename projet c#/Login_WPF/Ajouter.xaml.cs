﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vue
{
    /// <summary>
    /// Interaction logic for Ajouter.xaml
    /// </summary>
    public partial class Ajouter : Window
    {
        public Ajouter()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (name.Text == ""|| prenom.Text == ""|| adresse.Text == ""|| email.Text == ""|| tel.Text == "") {
                errormsg.Text = "Veuillez remplir tout les champs !";
            }
            else {
                Bll.Ajouter.AjouterNouveauContact(new Modele.Contact()
                {
                    Nom = name.Text,
                    Prenom = prenom.Text,
                    Adresse = adresse.Text,
                    Email = email.Text,
                    NumeroTel = tel.Text
                }, Vue.SeConnecter.utilisateurEnCours);
                errormsg.Text = "";
                succesmsg.Text = "Le nouveau contact a bien ete enregistrer !";
                name.Text = "";
                prenom.Text = "";
                adresse.Text = "";
                email.Text = "";
                tel.Text = "";
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Ajouter aa = new Ajouter();
            this.Close();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Menu ajout = new Menu();
            ajout.Show();
            this.Close();
        }
    }
}
