﻿using System.Windows;


namespace Vue {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Bt1_Click(object sender, RoutedEventArgs e) {
            SeConnecter con = new SeConnecter();
            Close();
            con.Show();
        }

        private void Bt3_Click(object sender, RoutedEventArgs e) {
            InscriptionUtilisateur nouveau = new InscriptionUtilisateur();
            Close();
            nouveau.Show();
            
        }

        private void Bt2_Click(object sender, RoutedEventArgs e) {
            Application.Current.Shutdown();
        }
    }
}
