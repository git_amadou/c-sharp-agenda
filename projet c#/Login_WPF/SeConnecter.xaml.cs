﻿using System.IO;
using System.Windows;
using System.Text.RegularExpressions;
using Bll;

namespace Vue {

    
    public partial class SeConnecter : Window {
        public static Modele.Utilisateur utilisateurEnCours = new Modele.Utilisateur();
        public SeConnecter(){
            InitializeComponent();
        }
        
        
        private void button1_Click(object sender, RoutedEventArgs e) {
            if (textBoxEmail.Text.Length == 0) {
                errormessage.Text = "veuillez entrer un E-mail .";
                textBoxEmail.Focus();
            }
            else if (!Regex.IsMatch(textBoxEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")){
                errormessage.Text = "veuillez entrer un E-mail Valide.";
                textBoxEmail.Select(0, textBoxEmail.Text.Length);
                textBoxEmail.Focus();
            }
            else {
                
                if (passwordBox1.Password.Length == 0) {
                    errormessage.Text = "Veuillez saisir un mot de passe ";
                }
                else if(Chercher.ConfirmerMotDePasse(textBoxEmail.Text , passwordBox1.Password) == false) {
                    errormessage.Text = "Le mot de passe ou l E-mail est incorrect !";
                    textBoxEmail.Select(0, textBoxEmail.Text.Length);
                    textBoxEmail.Focus();
                }
                else {
                    utilisateurEnCours = new Modele.Utilisateur() { Email = textBoxEmail.Text };
                    Bienvenue bn = new Bienvenue();
                    Close();
                    bn.Show();
                }    
            }
        }
        private void buttonRegister_Click(object sender, RoutedEventArgs e) {
            InscriptionUtilisateur registration = new InscriptionUtilisateur();
            Close();
            registration.Show();
            
        }
    }
}