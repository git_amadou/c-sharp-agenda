﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele {
    class Program
    {
        public static void Main()
        {

        }
    }

    public class Contact {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Adresse { get; set; }
        public string NumeroTel { get; set; }
        public uint ID { get; set; }
        public static uint nombresDeContacts = 0;

        public Contact() {
            ID++;
        }
        public Contact(string nom, string prenom, string adresse, string numeroTel, string email) {
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumeroTel = numeroTel;
            Email = email;
            nombresDeContacts++;
            ID++;
        }
    }
    
    public class Utilisateur {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Adresse { get; set; }
    }
}
