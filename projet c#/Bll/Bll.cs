﻿using Modele;
using DAL;
using System.Collections.Generic;
namespace Bll {
    class Program {
        public static void Main() {
            
        }
    }
    public static class Ajouter {
        public static void AjouterNouveauUtilisateur(Utilisateur nv) {
            Enregistrement.EnregistrerNouveauUtilisateur(nv);
        }
        public static void AjouterNouveauContact (Contact c , Utilisateur u) {
            Enregistrement.EnregistrerNouveauContact(c , u);
        }
    }
    public static class Chercher {
        public static bool ConfirmerMotDePasse(string mail, string mdpSaisie) {
            if (DAL.Chercher.ValiderUtilisateur(mail) != null) {
                if((DAL.Chercher.ValiderUtilisateur(mail)).Password != mdpSaisie) {
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
        public static List<Contact> ChargerLesContacts(Utilisateur u) {
            return DAL.Chercher.ChercherLesContactDunUtilisateur(u);
        }
    }
    public static class Supprimer {
        public static void SuprimerContact(Contact c , Utilisateur u) {
            DAL.Supprimmer.SupprimmerUnContact(c, u);
        }
    }
    }
