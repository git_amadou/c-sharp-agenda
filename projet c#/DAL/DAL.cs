﻿using System.IO;
using Modele;
using System.Collections.Generic;

namespace DAL { 

    static public class Enregistrement {
        public static void EnregistrerNouveauUtilisateur(Utilisateur nv) {
            
            Directory.CreateDirectory("../Utilisateurs/"+nv.Email);
            File.Create("../Utilisateurs/" + nv.Email + "/Contacts");
           // File.Create("../Utilisateurs/" + nv.Email + "/InfoPerso");
            File.AppendAllText("../testc", nv.Email + "/" + nv.Password +
                "/" + nv.Nom + "/" + nv.Prenom + "/" + nv.Adresse + '\n');
            File.Copy("../testc", "../Utilisateurs/" + nv.Email + "/InfoPerso");
           // File.AppendAllText("../Utilisateurs/" + nv.Email + "/InfoPerso", nv.Email + "/" + nv.Password + 
             //   "/"+nv.Nom+"/"+nv.Prenom+"/"+nv.Adresse+'\n');
        }
        public static void EnregistrerNouveauContact(Contact c , Utilisateur u) {
            File.AppendAllText("../Utilisateurs/" + u.Email + "/Contacts", c.Email + "/" + c.Nom + "/" + c.Prenom + "/" + c.NumeroTel + "/" + c.Adresse +'\n');
        }
    }
    public static class Supprimmer {
        public static void SupprimmerUnContact(Contact ct, Utilisateur u) {
            List<Contact> l = Chercher.ChercherLesContactDunUtilisateur(u);
            foreach (Contact c in l) {
                if (c == ct) {
                    l.Remove(c);
                }
            }
            foreach (Contact c in l){
                File.Delete("../Utilisateurs/" + u.Email + "/Contacts");
                File.AppendAllText("../Utilisateurs/" + u.Email + "/Contacts", c.Email + "/" + c.Nom + "/" + c.Prenom + "/" + c.NumeroTel + "/" + c.Adresse + '\n');
                
            }
        }
    }
    public static class Chercher {
        
        public static Contact ChercherUnContact() {
            //todo
            return null;
        }
        public static Utilisateur ValiderUtilisateur (string email) {
            if (File.Exists("../Utilisateurs/" + email + "/InfoPerso")) {
                Utilisateur u = new Utilisateur();
                string[] str = File.ReadAllLines("../Utilisateurs/" + email + "/InfoPerso");
                foreach (string st in str) {
                    string[] stt = st.Split('/');
                    Utilisateur ut = new Utilisateur() { Nom = stt[2], Prenom = stt[3], Password = stt[1], Email = stt[0] };
                    u = ut;
                }

                return u;
                
            }
            else {
                return null;
            }
        }
        public static List<Contact> ChercherLesContactDunUtilisateur (Utilisateur ut) {
            List<Contact> list = new List<Contact>();
            string[] str = File.ReadAllLines("../Utilisateurs/" + ut.Email + "/Contacts");
            foreach (string st in str) {
                string[] strr = st.Split('/');
                Contact contact = new Contact(strr[1], strr[2], strr[4], strr[3], strr[0]);
                list.Add(contact);
            }
            return list;
        } 
    }
    class Program {
        public static void Main(){

        }
    }

}
